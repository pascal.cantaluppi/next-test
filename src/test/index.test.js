import React from "react";
import App from "../../pages/index";
import { shallow } from "enzyme";
import renderer from "react-test-renderer";

describe("App component", () => {
  // it("Renders without crashing", () => {
  //   const wrapper = shallow(<App />);
  //   const text = wrapper.find("h1").text();
  //   expect(text).toEqual("Welcome to Next.js");
  // });

  it("matches the snapshot", () => {
    const tree = renderer.create(<App />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
