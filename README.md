This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Testing

Execute the test-suite with this command:

```bash
npm test
```

<p>
<img src="https://gitlab.com/pascal.cantaluppi/next-test/-/raw/master/public/airbnb.png" alt="airbnb" />
</p>
