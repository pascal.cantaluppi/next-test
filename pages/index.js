import Head from "next/head";
import IndexStyles from "../styles/IndexStyles";

export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>Next.js Testing</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="main">
        <h1 className="title">Welcome to awesome Next.js</h1>
      </main>
      <style jsx>{IndexStyles}</style>
    </div>
  );
}
